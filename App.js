import React from 'react';
import {ToastAndroid} from 'react-native';
import { StyleSheet, Text, View, TextInput, Image, Button } from 'react-native';

let pic = 
{
  uri: 'http://fc04.deviantart.net/fs71/f/2013/324/2/c/ember_prime_by_glenn144-d6uxs8b.jpg'
};

export default class App extends React.Component {

  testing1 = () => {
    console.log("testing1 reached");
  };
  constructor(props){
    super(props);
    this.state = {
      searchString: 'Enter Text'
    };
  }

  _onSearchTextChanged = (event) => {
    console.log('_onSearchTextChanged');
    this.setState({ searchString: event.nativeEvent.text});
  };

  _onSearchPressed = () => {
    const query = urlForQueryAndPage('place_name', this.state.searchSting, 1);
    this._executeQuery(query);
  }
  returnapiurl = () => {
    const url = 'http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/';
    return url;
  }

  _executeQuery = (apiurl) => {
    console.log("reached _executeQuery and the query is " + apiurl);

    fetch(apiurl)
    .then((response) => response.json())
    .then(console.log(response))
    .catch(error =>
       {
         console.log("something went wrong with fetch");
       });
  }

  XMLQuery = (apiurl) => {
    var request = new XMLHttpRequest();
    request.onreadystatechange = (e) => {
      if (request.readyState !== 4) {
        return;
      }
    
      if (request.status === 200) {
        console.log('success', request.responseText);
      } else {
        console.warn('error');
      }
    };
    
    request.open('GET', 'http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/');
    request.send();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize:25}}>Warframe Ember Prime</Text>
        <TextInput
            style={styles.searchInput}
            value={this.state.searchString}
            onChange={this._onSearchTextChanged}
            placeholder='Search here'/>
        <Button
        onPress={() => {
             console.log("entered value is "+this.state.searchString);
             ToastAndroid.show('Wubbalubbadubdub!!', ToastAndroid.SHORT);
             this.testing1();
             
           }}
      title='Search'
      />
      <Button
            onPress={() => {
              console.log("button 2 pressed");
              const apiurl = this.returnapiurl();
              console.log("URL for API is "+apiurl);
              //this._executeQuery(apiurl);
              this.XMLQuery(apiurl);
            }}
            color='#48BBEC'
            title='Hit me 2'
          />
        <Image source={pic} style={styles.container2}/>
        
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container2:
  {
    
    width: 30, 
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom:50,
  },
  flowRight:{
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  searchInput: {
    height: 46,
    width: 200,
    padding: 10,
    marginRight: 5,
    flexGrow: 1,
    fontSize: 18,
    borderWidth: 5,
    borderColor: '#48BBEC',
    borderRadius: 1,
    color: '#48BBEC',
  },
});
